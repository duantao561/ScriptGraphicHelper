# ScriptGraphicHelper

**一款简单好用的图色助手,  快速生成多种脚本开发工具的图色格式代码**

&nbsp;

# 功能

- 模拟器模式: 调用模拟器命令行进行截图, 无需手动连接adb(适用于雷电、夜神、逍遥)
- tcp连接模式: 与设备通过tcp通信进行截图(需要安装截图助手.apk)
- aj连接模式: 调用aj的vscode tcp端口进行截图(需要开启aj的悬浮窗)
- adb连接模式: 与设备通过adb进行截图(usb/wifi)
- 句柄模式: 调用大漠进行前后台截图
- 支持大漠、按键、触动、autojs、easyclick 以及自定义的格式代码生成
- 多分辨率适配的测试和代码生成(锚点格式)

&nbsp;

[下载地址](https://gitee.com/yiszza/ScriptGraphicHelper/releases)



## **支持平台**

- win：  所有功能

- mac： aj连接模式, tcp模式

- linux：未支持, 存在的问题([#4427](https://github.com/AvaloniaUI/Avalonia/issues/4427))


&nbsp;

&nbsp;

# 展示



![](screenshot/record.gif)


&nbsp;

&nbsp;

