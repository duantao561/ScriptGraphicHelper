using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Threading;
using System;

namespace ScriptGraphicHelper.Views
{
    public partial class MessageBox : Window
    {
        public static async void ShowAsync(string msg)
        {
            await Dispatcher.UIThread.InvokeAsync(async () => {
                await new MessageBox(msg).ShowDialog(MainWindow.Instance);
            });
        }

        public static async void ShowAsync(string title, string msg)
        {
            await Dispatcher.UIThread.InvokeAsync(async () => {
                await new MessageBox(title, msg).ShowDialog(MainWindow.Instance);
            });
        }

        public MessageBox()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private new string Title { get; set; } = string.Empty;
        private string Message { get; set; } = string.Empty;

        public MessageBox(string title, string msg) : this()
        {
            Title = title;
            Message = msg;

            ExtendClientAreaToDecorationsHint = true;
            ExtendClientAreaTitleBarHeightHint = -1;
            ExtendClientAreaChromeHints = Avalonia.Platform.ExtendClientAreaChromeHints.NoChrome;
        }

        public MessageBox(string msg) : this("��ʾ", msg) { }


        private void Window_Opened(object sender, EventArgs e)
        {
            var title = this.FindControl<TextBlock>("Title");
            title.Text = Title;
            var tb = this.FindControl<TextBlock>("Message");
            tb.Text = Message;
        }

        private async void Close_Tapped(object sender, RoutedEventArgs e)
        {
            await Application.Current.Clipboard.SetTextAsync(Message);
            this.Close();
        }

        private async void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                await Application.Current.Clipboard.SetTextAsync(Message);
                this.Close();
            }
        }

        private void Window_PropertyChanged(object sender, AvaloniaPropertyChangedEventArgs e)
        {
            if (e.Property.Name == "Width" || e.Property.Name == "Height")
            {
                Position = new PixelPoint((int)(Screens.Primary.WorkingArea.Width / 2 - Width / 2), (int)(Screens.Primary.WorkingArea.Height / 2 - Height / 2));
            }
        }
    }
}
